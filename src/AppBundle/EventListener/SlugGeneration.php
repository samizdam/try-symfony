<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\AbstractArticle;
use AppBundle\Service\Slug\SlugGeneratorInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class SlugGeneration implements EventSubscriber
{

    /**
     * @var SlugGeneratorInterface
     */
    private $slugGenerator;

    public function __construct(SlugGeneratorInterface $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->setSlugValue($args->getObject());
    }

    private function setSlugValue($object)
    {
        if ($object instanceof AbstractArticle) {
            $object->setSlug($this->slugGenerator->generateSlug($object->getName()));
        }
    }
}