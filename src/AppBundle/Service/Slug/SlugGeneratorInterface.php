<?php

namespace AppBundle\Service\Slug;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface SlugGeneratorInterface
{
    public function generateSlug(string $string): string;
}