<?php

namespace AppBundle\Service\Transliterator;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
interface TransliteratorInterface
{

    public function tranliterate(string $string): string;

}